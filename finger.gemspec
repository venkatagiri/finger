# frozen_string_literal: true

require_relative 'lib/finger/version'

Gem::Specification.new do |gem|
  gem.name          = 'finger'
  gem.summary       = 'ruby implementation of Finger Protocol RFC1288'
  gem.authors       = 'venkatagiri'
  gem.homepage      = 'https://gitlab.com/venkatagiri/finger'
  gem.licenses      = ['MIT']
  gem.executables   = ['finger']
  gem.files         = Dir['lib/**/*']
  gem.version       = Finger::VERSION

  gem.required_ruby_version = '~> 2.5'
  gem.add_runtime_dependency 'bindata', '~> 2.4.10'

  gem.add_development_dependency 'bundler', '~> 1.16.6'
  gem.add_development_dependency 'rake', '~> 13.0.6'
  gem.add_development_dependency 'rspec', '~> 3.10.0'
  gem.add_development_dependency 'rubocop', '~> 1.23.0'
  gem.metadata = {
    'rubygems_mfa_required' => 'true'
  }
end
