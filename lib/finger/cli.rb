# frozen_string_literal: true

require 'socket'

module Finger
  # `finger` cli
  class CLI
    FINGER_PORT = 79

    def call(query = nil)
      log "cli request for query: #{query}"

      # anonymous query
      return list_all if query.nil?

      # remote query
      if query.include? '@'
        remote query
      else # user query
        users = Finger::Passwd.fetch query
        return "[finger] #{query}: no such user." if users.empty? || query.empty?

        users.map do |user|
          user_info user
        end.join "\n\n"
      end
    end

    private

    def remote(query)
      index = query.rindex '@'
      remote_query = query[0..index - 1]
      remote_host = query[index + 1..-1]

      TCPSocket.open remote_host, FINGER_PORT do |socket|
        socket.puts remote_query
        response = ["host: #{remote_host}"]
        while (line = socket.gets)
          response << line.strip
        end

        response.join("\n")
      end
    end

    def list_all
      return 'No one logged on.' if logins.empty?

      result = []
      result << ['Login', 'TTY', 'Idle', 'Login Time', 'From']
      logins.each do |rec|
        result << [rec.login, rec.tty, rec.idle_time, rec.login_time, rec.hostname]
      end

      result.map do |res|
        res.join "\t"
      end.join "\n"
    end

    def user_info(user)
      result = []
      result << "Login: #{user.login}\tName: #{user.name}"
      result << "Directory: #{user.home}\tShell: #{user.shell}"

      user_logins = logins.select { |l| l.login == user.login }
      user_logins.each do |l|
        result << "On since #{l.login_time} on #{l.tty} from #{l.hostname}"
      end
      result << 'Never logged in.' if user_logins.empty?

      result.join "\n"
    end

    def logins
      @logins ||= Finger::UTMP.active_logins
    end

    def log(msg)
      warn "[#{Time.now}][finger] #{msg}" if ENV['DEBUG']
    end
  end
end
