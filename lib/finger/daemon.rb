# frozen_string_literal: true

require 'socket'

module Finger
  # Network utility to handle remote request for user info
  class Daemon
    attr_reader :host, :port

    def initialize(host = 'localhost', port = 79)
      @host = host
      @port = port

      @shutdown = false
    end

    def run(background: true)
      if background
        @thread = Thread.new { start }
      else
        start
      end
    end

    def stop
      log 'stopping...'
      @shutdown = true
      @thread&.join
    end

    private

    def start
      log 'starting...'

      Signal.trap('TERM') { stop }
      Signal.trap('INT')  { stop }

      server = TCPServer.open @host, @port
      until @shutdown
        begin
          conn = server.accept_nonblock
        rescue IO::EAGAINWaitReadable
          next
        end

        Thread.start(conn) do |client|
          response = finger client.gets.strip
          client.puts response
          client.close
        end
      end
      server.close
    end

    def finger(query)
      log "got request for #{query}"
      # TODO: delegate to local system finger
      CLI.new.call query
    end

    def log(msg)
      warn "[#{Time.now}][fingerd] #{msg}" if ENV['DEBUG']
    end
  end
end
