# frozen_string_literal: true

module Finger
  # Parse user info from the `passwd` file
  class Passwd
    FILE = '/etc/passwd'
    User = Struct.new(:login, :name, :home, :shell)

    def self.fetch(query)
      parse.select { |u| u.login.match query }
    end

    def self.parse
      File
        .foreach(FILE)
        .map do |line|
          user, _passwd, _uid, _gid, info, home, shell = line.split(':')
          User.new(user, info.split(',').first, home, shell.strip)
        end
    end
  end
end
