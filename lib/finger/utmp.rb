# frozen_string_literal: true

require 'bindata'

module Finger
  UT_LOGIN  = 7
  UT_DEAD   = 8

  # Login time of the user
  class LoginTime < BinData::Primitive
    endian :little
    uint32 :seconds
    uint32 :useconds

    def get
      Time.at(seconds)
    end
  end

  # IP Address
  class IPAddr < BinData::Primitive
    array :octets, type: :uint8, initial_length: 16

    def get
      octets[0..3].map { |oct| format('%d', oct) }.join('.')
    end
  end

  # Holds each utmp record
  class Record < BinData::Record
    endian :little

    uint32 :login_type
    uint32 :pid
    string :line, length: 32
    string :id,   length: 4
    string :user, length: 32
    string :host, length: 256
    struct :exit do
      uint16 :term_status
      uint16 :exit_status
    end
    uint32 :session
    login_time :login_time
    ip_addr :ip_address
    string :reserved, read_length: 20

    def active?
      login_type == UT_LOGIN
    end

    def tty
      line.strip
    end

    def login
      user.strip
    end

    def hostname
      host.strip
    end

    def idle_time
      idle = File.exist?("/dev/#{tty}") ? (Time.now - File.stat("/dev/#{tty}").mtime).to_i : 0

      [format('%02d', (idle / 60)), format('%02d', (idle % 60))].join(':')
    end
  end

  # Parser for user logins from utmp file
  class UTMP < BinData::Record
    FILE = '/var/run/utmp'
    array :records, type: :record, read_until: :eof

    def self.parse
      read(File.read(FILE)).records
    end

    def self.active_logins
      parse.select(&:active?)
    end
  end
end
