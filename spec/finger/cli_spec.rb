# frozen_string_literal: true

require 'finger'

### BNF for Finger
# {Q1}    ::= [{W}|{W}{S}{U}]{C}
# {Q2}    ::= [{W}{S}][{U}]{H}{C}
# {U}     ::= username
# {H}     ::= @hostname | @hostname{H}
# {W}     ::= /W
# {S}     ::= <SP> | <SP>{S}
# {C}     ::= <CRLF>

RSpec.describe Finger::CLI do
  before do
    stub_const('Finger::UTMP::FILE', 'spec/fixtures/files/utmp.multiple.dat')
    stub_const('Finger::Passwd::FILE', 'spec/fixtures/files/passwd')
  end

  describe 'with a null command ({C})' do
    context 'when no one is logged on' do
      before do
        stub_const('Finger::UTMP::FILE', 'spec/fixtures/files/utmp.zero.dat')
      end

      it 'shoud say no logons' do
        result = subject.call
        expect(result).to eq 'No one logged on.'
      end
    end

    context 'when someone is logged on' do
      it 'shoud have a header' do
        header = subject.call.lines.first
        expect(header).to match(/^Login/)
        expect(header).to match(/TTY/)
        expect(header).to match(/Idle/)
        expect(header).to match(/Login Time/)
      end

      it 'shoud list logged in users' do
        result = subject.call.lines

        expect(result.length).to eq 6
        expect(result[1]).to match(/^grant/)
        expect(result[1]).to match(/65.93.202.213/)
        expect(result[4]).to match(/^sattler/)
        expect(result[5]).to match(/web console/)
      end
    end
  end

  context 'with name specified ({U}{C})' do
    it 'shoud show error for empty("") query' do
      result = subject.call ''
      expect(result).to match(/ : no such user.$/)
    end

    it 'shoud not list missing user' do
      result = subject.call 'missing'
      expect(result).to match(/missing: no such user.$/)
    end

    it 'shoud list user info' do
      result = subject.call 'sattler'
      expect(result.lines.length).to be 3
      expect(result).to match(/^Login: sattler/)
      expect(result).to match(/Name: Ellie Sattler$/)
      expect(result).to match(%r{^Directory: /home/sattler})
      expect(result).to match(%r{Shell: /bin/bash$})
      expect(result).to match(/^On since/)
      expect(result).to match(%r{on pts/4 from 65.93.202.213$})
    end

    it 'shoud show message if never logged in' do
      result = subject.call 'grand'
      expect(result).to match(/^Never logged in./)
    end
  end

  context 'with ambiguous name specified' do
    it 'shoud list all users with matching name' do
      result = subject.call 'gran'
      expect(result).to match(/^Login: grant/)
      expect(result).to match(/^Login: grand/)
    end
  end

  context 'with query by hostname' do
    before do
      @server = Finger::Daemon.new 'localhost', 1179
      stub_const('Finger::CLI::FINGER_PORT', @server.port)

      @server.run
      sleep 0.15
    end
    after do
      @server.stop
    end

    it 'shoud list user info from remote host' do
      result = subject.call 'grant@localhost'
      expect(result).to match(/^Login: grant/)
    end

    it 'shoud handle multiple hostnames' do
      result = subject.call 'grant@localhost@localhost'
      expect(result).to match(/^Login: grant/)
    end
  end
end
