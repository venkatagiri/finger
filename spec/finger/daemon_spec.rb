# frozen_string_literal: true

require 'socket'
require 'finger'

RSpec.describe Finger::Daemon do
  subject do
    Finger::Daemon.new 'localhost', 1179
  end

  context 'when started' do
    before do
      stub_const('Finger::UTMP::FILE', 'spec/fixtures/files/utmp.multiple.dat')
      stub_const('Finger::Passwd::FILE', 'spec/fixtures/files/passwd')

      subject.run
      sleep 0.15
    end
    after do
      subject.stop
    end

    it 'shoud listen for incoming user requests' do
      socket = TCPSocket.open subject.host, subject.port
      socket.puts 'grant'
      result = socket.gets
      expect(result).to match(/^Login: grant/)
    end
  end
end
