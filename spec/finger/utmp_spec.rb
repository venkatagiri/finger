# frozen_string_literal: true

require 'finger'

RSpec.describe Finger::Record do
  context 'when utmp file is zero byte' do
    let(:zero_records) { '' }

    it 'shoud raise error' do
      expect { subject.read zero_records }.to raise_error(EOFError)
    end
  end

  context 'when utmp file has one record' do
    before do
      @file = File.open('spec/fixtures/files/utmp.one.dat')
    end
    after do
      @file.close
    end

    it 'shoud read the first record and fail next' do
      expect(subject.read(@file)).to_not be_nil
      expect { subject.read @file }.to raise_error(EOFError)
    end

    it 'shoud parse the fields correctly' do
      record = subject.read @file

      expect(record.active?).to be true
      expect(record.tty).to eq 'pts/5'
      expect(record.login).to eq 'grant'
      expect(record.ip_address).to eq '65.93.202.213'
      expect(record.login_time.to_i).to eq 1_636_854_168
    end
  end

  context 'when utmp file has multiple records' do
    before do
      @file = File.open('spec/fixtures/files/utmp.multiple.dat')
    end
    after do
      @file.close
    end

    it 'shoud read complete file' do
      records = []
      records << subject.read(@file) until @file.eof?
      expect(records.length).to be 42
    end
  end
end
